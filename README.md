# Pokémon Trainer web app using Angular Framework

## Table of Contents

- [About](#about)
- [Install](#install)
- [Usage](#usage)
- [Built with](#built-with)
- [Contributing](#contributing)

## About

This project is about building a Pokémon Trainer web app using the Angular Framework. The application communicates with the server through REST API : https://gt-pokemon-production.up.railway.app/ to store trainers and pokemons collected. The available pokemons and information about pokemons is found from the REST API: https://pokeapi.co/

### Appendix A: Requirements for the Pokemon Trainer Application

The application allows a user to collect Pokémon received from the PokeAPI. Users must enter username before being able to collect any Pokémon. Users must also be able to view the Pokémon that have been collected.
The application contains three main screens:

#### Login page

The first thing a user should see is the “Login page” where the user must be able to enter their “Trainer” name.
There should be a button that saves the Trainer name to the Trainer API and in local- or sessionStorage. The app must
then be redirected to the main page, the Pokémon Catalogue page.
The users may NOT be able to see the Pokémon Catalogue without have a Trainer name stored in local- or sessionStorage.
Use a Guard service to achieve this functionality.
If username exists in local- or sessionStorage, you may automatically redirect from the landing page to the Pokémon Catalogue page.
You can first check if the username exists in the Trainer API before redirecting to the Catalogue page.

#### Trainer Page

A user may only view this page if there is a Trainer name that exists in localStorage. Please redirect a user back to the
Landing page if they do not have a Trainer name stored in localStorage. Use a Guard service to achieve this functionality.
The Trainer page should list the Pokémon that the trainer has collected. For each collected Pokémon, display the Pokémon
name and image.
A user must also be able to remove a Pokémon from their collection from the Trainer page.

#### Pokémon Catalogue Page

The Catalogue page may NOT be viewed if there is no Trainer name stored in localStorage. Use a Guard service to achieve this functionality.
The Catalogue page must list the Pokémon name and avatar\*.

## Install

To run the application locally: clone repository and build project. Make sure you have npm and Angular CLI installed. Please note an api-key is needed to connect to the server.

## Usage

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.

## Built with

- NPM/Node.js (LTS – Long Term Support version)
- Angular JS
- Angular CLI
- Tailwind
- TypeScript
- Visual Studio Code Text Editor
- Browser Developer Tools for testing and debugging
- Angular Dev Tool
- Git
- Rest API: https://gt-pokemon-production.up.railway.app/

## Contributing

- [Giorgos Tzafilkos](https://gitlab.com/tzaf.giorgos)
- [Kamperopoulos Anastasios](https://gitlab.com/KamperopoulosA)

PRs accepted.
