import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize } from 'rxjs';
import { environment } from 'src/environments/environment.development';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Pokemon } from '../models/pokemon.model';
import { Result } from '../models/result.model';
import { StorageUtil } from '../utils/storage.utils';
const {apiPokemons} = environment



@Injectable({
  providedIn: 'root'
})
export class PokemonCatalogueService {

  private  _pokemons: Pokemon[] = [];
  private _error: string = "";
  private _loading: boolean = false;

  get pokemons():Pokemon[]{
    return this._pokemons;
  }

  get error(): string{
    return this._error
  }

  get loading(): boolean{
    return this._loading
  }

  constructor(private readonly http: HttpClient) {
    const pokemons = StorageUtil.storageRead<Pokemon[]>(StorageKeys.Pokemons);
    if (pokemons) {
      this._pokemons = pokemons;
      this.findImagesAndId();
    }
  }

  /**
   * This method sends a http get request to load all 151 pokemons 
   * from the pokemon API. 
   */
  public findAllPokemons(): void {
    if (this._pokemons.length > 0 || this._loading) {
      return;
    }

    this._loading = true;
    this.http
      .get<Result>(apiPokemons)
      .pipe(
        finalize(() => {
          this._loading = false;
        })
      )
      .subscribe({
        next: (result: Result) => {
          this._pokemons = result.results;
          StorageUtil.storageSave<Pokemon[]>(
            StorageKeys.Pokemons,
            this._pokemons
          );
          this.findImagesAndId();
        },
        error: (error: HttpErrorResponse) => {
          this._error = error.message;
        },
      });
  }

  /**
   * This method finds all the images and ids of the 
   * pokemons fetched with the findAllPokemons() method. 
   */
  private findImagesAndId(): void {
    this._pokemons.forEach((pokemon) => {
      const lastPart = pokemon.url
        .split('/')
        .filter((e) => e)
        .slice(-1);
      if (lastPart.length > 0) {
        const lastElement = lastPart.pop();
        pokemon.image = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${lastElement}.png`;
        pokemon.id = `#${lastElement}`;
      } else {
        pokemon.image = 'No image found';
      }
    });
  }

  /**
   * This method returns the pokemon object or undefined
   * @param name Name of pokemon to be found
   * @returns the pokemon found, or undefined if not found
   */
  public pokemonByName(name: string): Pokemon | undefined {
    return this._pokemons.find((pokemon: Pokemon) => pokemon.name === name);
  }
}