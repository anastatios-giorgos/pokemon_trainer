import { Injectable } from '@angular/core';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Pokemon } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { StorageUtil } from '../utils/storage.utils';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _trainer?: Trainer

   get trainer(): Trainer | undefined{
    return this._trainer
  }

  set trainer(trainer: Trainer | undefined){
    StorageUtil.storageSave<Trainer>(StorageKeys.Trainer,trainer!)
    this._trainer = trainer
  }

  constructor() { 
    this._trainer = StorageUtil.storageRead<Trainer>(StorageKeys.Trainer)
    

  }

   /**
   * This method checks whether pokemon is in pokebag or not. 
   * @param name name of pokemon to be search for
   * @returns true is pokeman in pokebag, false if not. 
   */
   public inPokemons(name: string): boolean {
    if (this.trainer) {
      return Boolean(
        this.trainer?.pokemon.find((pokemon: Pokemon) => pokemon.name === name)
      );
    }
    return false;
  }

  /**
   * This method removes a pokemon from the pokebag
   * @param name Name of pokemon to be removed
   */
  public removeFromPokemons(name: string): void {
    if (this._trainer) {
      this._trainer.pokemon = this._trainer.pokemon.filter(
        (pokemon: Pokemon) => pokemon.name !== name
      );
    }
  }

  /**
   * This method adds a selected pokemon to the pokebag 
   * @param pokemon The pokemon to be added
   */
  public addToPokemons(pokemon: Pokemon): void {
    if (this._trainer) {
      this._trainer.pokemon.push(pokemon);
    }
  }

}
