import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment.development';
import { Pokemon } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { PokemonCatalogueService } from './pokemon-catalogue.service';
import { UserService } from './user.service';


const {apiKey,apiTrainers} = environment;

@Injectable({
  providedIn: 'root'
})
export class CatchService {

  private _loading: boolean = false;

  get loading(): boolean {
    return this._loading;
  }

  constructor(
    private http: HttpClient,
    private readonly trainerService: UserService,
    private readonly pokemonCatalogueService: PokemonCatalogueService
  ) {}
  
  /**
   * This method adds a pokemon to the trainer. 
   * @param name Name of pokemon to be stored
   * @returns an observable trainer. 
   */
  public addToPokemon(name: string): Observable<Trainer> {
    
    if (!this.trainerService.trainer) {
      alert('There is no user!');
      throw new Error('addToPokemon: There is no user!');
    }
    
    const trainer: Trainer = this.trainerService.trainer;
    const pokemon: Pokemon | undefined =
      this.pokemonCatalogueService.pokemonByName(name);
    
      if (!pokemon) {
      alert("Can't find the pokemon with name: " + name);
      throw new Error('addToPokemon: No pokemon with name: ' + name);
    }

    if (this.trainerService.inPokemons(name)) {
      alert('Pokemon already owned!');
    } else {
      this.trainerService.addToPokemons(pokemon);
      alert('You successfully caught ' + name + '!');
    }

    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': apiKey,
    });

    this._loading = true;

    return this.http
      .patch<Trainer>(
        `${apiTrainers}/${trainer.id}`,
        {
          pokemon: [...trainer.pokemon],
        },
        {
          headers,
        }
      )
      .pipe(
        tap((updatedTrainer: Trainer) => {
          this.trainerService.trainer = updatedTrainer;
        }),
        finalize(() => (this._loading = false))
      );
  }

  public removePokemon(name: string) {
    
    if (!confirm('Are you sure you want to release ' + name + '?')) return;

    if (!this.trainerService.trainer) {
      alert('There is no user!');
      throw new Error('addToPokemon: There is no user!');
    }
    
    const trainer: Trainer = this.trainerService.trainer;
    this.trainerService.removeFromPokemons(name);
    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': apiKey,
    });

    this._loading = true;

    return this.http
      .patch<Trainer>(
        `${apiTrainers}/${trainer.id}`,
        {
          pokemon: [...trainer.pokemon],
        },
        {
          headers,
        }
      )
      .pipe(
        tap((updatedTrainer: Trainer) => {
          this.trainerService.trainer = updatedTrainer;
        }),
        finalize(() => (this._loading = false))
      );
  }
}
