import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap } from 'rxjs';
import { environment } from 'src/environments/environment';

import { Trainer } from '../models/trainer.model';


const { apiTrainers,apiKey } = environment;

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  //Dependency Injection.
  constructor(private readonly http: HttpClient) { }

  //Models, HttpClient, Observables, and RxJs operators.
  public login(username:string): Observable<Trainer>{
    return this.checkUsername(username)
    .pipe(
      switchMap((trainer: Trainer | undefined) => {
        if(trainer === undefined){
          //user does not exist
          return this.createUser(username)
        }
        return of(trainer);
      })

    )
    
  }


 


  //check if user exists
  private checkUsername(username:string):Observable<Trainer | undefined>{
    return this.http.get<Trainer[]>(`${apiTrainers}?username=${username}`).pipe(
      //Rxjs operators
      map((response:Trainer[]) =>  response.pop())
      )

    
  }

  //create a user
  private createUser(username: string): Observable<Trainer>{
    //user
    const trainer ={
      username,
      pokemon: [],
    };
    //headers -> API Key
    const headers = new HttpHeaders({
      "Content-Type" : "application/json",
      "x-api-key": apiKey
    });
    // POST- Create items on the server
    return this.http.post<Trainer>(apiTrainers,trainer,{
      headers
    })
  }

  //if user || created user - store user
}
