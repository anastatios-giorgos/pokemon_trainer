import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { Trainer } from 'src/app/models/trainer.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css']
})
export class TrainerPage implements OnInit {
  get trainer(): Trainer | undefined {
    return this.trainerService.trainer;
  }

  get pokemons(): Pokemon[] {
    if (this.trainerService.trainer) {
      return this.trainerService.trainer?.pokemon;
    }
    return [];
  }

  constructor(private trainerService: UserService) {}

  ngOnInit(): void {}
}


