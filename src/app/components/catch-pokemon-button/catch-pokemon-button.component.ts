import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { CatchService } from 'src/app/services/catch.service';
import { UserService } from 'src/app/services/user.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-catch-pokemon-button',
  templateUrl: './catch-pokemon-button.component.html',
  styleUrls: ['./catch-pokemon-button.component.css']
})
export class CatchPokemonButtonComponent implements OnInit {

  public isCaught: boolean = false; 
  public isTrainer: boolean = false; 

  @Input() pokemonName: string = '';

  get loading(): boolean {
    return this.catchService.loading;
  }

  constructor(
    private trainerService: UserService,
    private readonly catchService: CatchService, 
    private location: Location
  ) {}

  ngOnInit(): void {
    this.isCaught = this.trainerService.inPokemons(this.pokemonName); 
    if (this.location.path()=== "/trainer") this.isTrainer = true;
  }

  /**
   * This method handles the event of an user 
   * clicking on the catch-pokemon button. 
   * Checks whether application should add or 
   * remove pokemon based on which page the click is 
   * coming from. 
   */
  onCatchClick(): void {
    
    //Add pokemon 
    if (this.location.path() === "/pokemons") {
      this.catchService.addToPokemon(this.pokemonName).subscribe({
        next: (trainer: Trainer) => {
          this.isCaught = this.trainerService.inPokemons(this.pokemonName)
        },
        error: (error: HttpErrorResponse) => {
          console.log('ERROR', error.message);
        },
      });
    }

    //remove pokemon 
    if (this.location.path() === "/trainer") {
      const trainer = this.catchService.removePokemon(this.pokemonName)
      if (trainer !== undefined) {
        trainer.subscribe({
          next: (trainer: Trainer) => {
            this.isCaught = false; 
          },
          error: (error: HttpErrorResponse) => {
            console.log('ERROR', error.message);
          },
        });;
      } 
    }
  }
}


